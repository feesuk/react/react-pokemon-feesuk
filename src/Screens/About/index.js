import React from "react";

export default function About() {
  return (
    <div className="about">
      <div className="about__header"></div>
      <div className="about__main-section"></div>
      <div className="about__footer"></div>
    </div>
  );
}
