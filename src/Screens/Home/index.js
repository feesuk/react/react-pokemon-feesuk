import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Layout from "Components/Layout";
import { getPokemon, setFavoritPokemon } from "Store/Action/Pokemon";
import { logIn, logOut } from "Store/Action/Auth";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      limit: 10,
      offset: 0,
    };
  }

  componentDidMount() {
    this.props.getPokemon({
      limit: this.state.limit,
      offset: this.state.offset,
    });
  }

  render() {
    const { offset } = this.state;
    const { pokemons } = this.props;

    return (
      <Layout>
        <div className="home">
          <div className="home__title">Poke Fav</div>
          {this.props.isAuthenticated ? (
            <button onClick={this.props.logOut}>Logout</button>
          ) : (
            <button onClick={this.props.logIn}>Login</button>
          )}

          <div className="home__grid container">
            {this.props.favorits?.length === 0
              ? null
              : this.props.favorits?.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset + index + 1}`}
                      >
                        <span>{pokemon?.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>

          <div className="home__title">Poke List</div>
          <div className="home__grid container">
            {pokemons.length === 0
              ? null
              : pokemons.map((pokemon, index) => {
                  return (
                    <div className="home__grid__item" key={index}>
                      <button
                        className="home__grid__item__save"
                        onClick={() =>
                          this.props.setFavoritPokemon({
                            id: offset + index + 1,
                            name: pokemon.name,
                          })
                        }
                      >
                        +
                      </button>

                      <Link
                        className="home__grid__item__content"
                        to={`/pokemon/${offset + index + 1}`}
                      >
                        <span>{pokemon.name}</span>
                      </Link>
                    </div>
                  );
                })}
          </div>
        </div>
      </Layout>
    );
  }
}

//map state to props adalag fungsi untuk me-mapping data dari store-redux ke komponen yang menggunakan (terbaca sebagai props)
const mapStateToProps = (state) => {
  return {
    favorits: state.Pokemon.favoritPokemon,
    pokemons: state.Pokemon.pokemons,
    isAuthenticated: state.Auth.isAuthenticated,
  };
};

//export default connect(mapping-state-to-props, mapping-dispatch)(component-name)
export default connect(mapStateToProps, {
  getPokemon,
  setFavoritPokemon,
  logIn,
  logOut,
})(Home);
