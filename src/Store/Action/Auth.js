import Cookies from "js-cookie";

export const logIn = () => (dispatch) => {
  /**
   * step login
   * - fetch POST API dengan memasukan username/password di dalam body
   * - response dari API akan memberikan token
   * - token di simpan di browser-storage(localStorage atau Cookies) serta di global-state (eg:redux)
   */

  const fakeToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9";
  Cookies.set("token", fakeToken, { expires: 7 });

  dispatch({
    type: "SIGN_IN",
    payload: {
      token: fakeToken,
    },
  });
};

export const logOut = () => (dispatch) => {
  Cookies.remove("token");

  dispatch({
    type: "SIGN_OUT",
  });
};
