import Cookies from "js-cookie";

const initialState = {
  isAuthenticated: Cookies.get("token") ? true : false,
  token: Cookies.get("token"),
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "SIGN_IN":
      return { ...state, isAuthenticated: true, token: action.payload.token };
    case "SIGN_OUT":
      return { ...state, isAuthenticated: false, token: undefined };
    default:
      return state;
  }
};
