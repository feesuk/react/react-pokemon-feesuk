import { combineReducers } from "redux";

import Pokemon from "./Pokemon";
import Auth from "./Auth";

export default combineReducers({ Pokemon, Auth });
